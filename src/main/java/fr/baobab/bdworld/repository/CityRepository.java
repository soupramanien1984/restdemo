package fr.baobab.bdworld.repository;

import org.springframework.data.repository.CrudRepository;

import fr.baobab.bdworld.model.City;

public interface CityRepository extends CrudRepository<City, Long> {

}
