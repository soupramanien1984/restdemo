package fr.baobab.bdworld.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.baobab.bdworld.exception.CityNotFoundException;
import fr.baobab.bdworld.exception.NoDataFoundException;
import fr.baobab.bdworld.model.City;
import fr.baobab.bdworld.repository.CityRepository;
import fr.baobab.bdworld.service.ICityService;

@Service
public class CityService implements ICityService {

	@Autowired
    private CityRepository cityRepository;

    @Override
    public City findById(Long id) throws CityNotFoundException {

        return cityRepository.findById(id)
                .orElseThrow(()->new CityNotFoundException(id));
    }

    @Override
    public City save(City city) {

        return cityRepository.save(city);
    }

    @Override
    public List<City> findAll() {

         List<City> cities = (List<City>) cityRepository.findAll();

        if (cities.isEmpty()) {

            throw new NoDataFoundException();
        }

        return cities;
    }

}
