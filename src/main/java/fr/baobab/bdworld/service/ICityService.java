package fr.baobab.bdworld.service;

import java.util.List;

import fr.baobab.bdworld.model.City;

public interface ICityService {
	City findById(Long id);
    City save(City city);
    List<City> findAll();
}
